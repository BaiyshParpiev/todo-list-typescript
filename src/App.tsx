import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Navbar from "./component/UI/Navbar/Navbar";
import AboutPage from "./component/AboutPage/AboutPage";
import TodoPage from "./component/TodoPage/TodoPage";


const App: React.FC = () => {
    return (
        <BrowserRouter>
            <Navbar/>
            <div className="container">
                <Switch>
                    <Route component={TodoPage} exact path='/'/>
                    <Route component={AboutPage} path='/about'/>
                </Switch>
            </div>
        </BrowserRouter>
    );
};

export default App;