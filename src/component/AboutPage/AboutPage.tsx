import React from 'react';
import {useHistory} from "react-router-dom";

const AboutPage:React.FC = () => {
    const history = useHistory();
    return (
        <>
            <h1>Information page</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur distinctio doloremque, dolorum ea esse, fuga labore nam obcaecati perferendis perspiciatis quia quo, tenetur? A blanditiis culpa deleniti dolores ex id ipsum, magnam mollitia nemo odit porro possimus repellat saepe sint.</p>
            <button className='btn' onClick={() => history.push('/')}>Back to tasks</button>
        </>
    );
};

export default AboutPage;