import React, {useEffect, useState} from 'react';
import TodoForm from "../TodoForm/TodoForm";
import TodoList from "../TodoList/TodoList";
import {ITodo} from "../../interfaces";

declare var confirm: (question: string) => boolean

const TodoPage: React.FC = () => {
    const [todos, setTodos] = useState<ITodo[]>([]);

    useEffect(() => {
        const saved = JSON.parse(localStorage.getItem('todos') || '[]') as ITodo[];
        setTodos(saved);
    }, []);

    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify(todos))
    }, [todos])

    const addHandler = (title: string) => {
        const newTodo = {
            title: title,
            id: Date.now(),
            completed: false
        }
        setTodos(prev => [newTodo, ...prev])
    };

    const toggleHandler = (id: number) => {
        setTodos(prev =>
            prev.map(p => {
                if (p.id === id) {
                    p.completed = !p.completed;
                }
                return p;
            })
        )
    }

    const removeHandler = (id: number) => {
        const shouldRemove = confirm('Are you sure, that you want to delete?');
        if (shouldRemove) {
            setTodos(prev => prev.filter(p => p.id !== id));
        }
    }

    return <React.Fragment>
        <TodoList
            todos={todos}
            onToggle={toggleHandler}
            onRemove={removeHandler}
        />
        <TodoForm
            onAdd={addHandler}
        />
    </React.Fragment>
}

export default TodoPage;