import React from 'react';
import {NavLink} from "react-router-dom";

export const Navbar:React.FC = () => {
    return (
        <nav>
            <div className="nav-wrapper px1">
                <a href="/" className="brand-logo">Tasks list</a>
                <ul className="right hide-on-med-and-down">
                    <li><NavLink to="/">Tasks</NavLink></li>
                    <li><NavLink to="/about">Info</NavLink></li>
                </ul>
            </div>
        </nav>
    );
};

export default Navbar;