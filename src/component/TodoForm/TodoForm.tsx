import React, {useState} from 'react';

type Props = {
    onAdd(title: string): void;
}

const TodoForm: React.FC<Props> = ({onAdd}) => {
    const [title, setTitle] = useState<string>('');
    const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTitle(event.target.value);
    };

    const keyPress = (event: React.KeyboardEvent) => {
        if(event.key === "Enter"){
            onAdd(title);
            setTitle('');
        }
    }

    return (
        <div className="input-field mt2">
            <input
                onChange={changeHandler}
                value={title}
                type="text"
                id="title"
                placeholder=" Write your plan"
                onKeyPress={keyPress}
            />
            <label htmlFor="title" className="active">
                Write your plan
            </label>
        </div>
    );
};

export default TodoForm;